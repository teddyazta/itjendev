<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Inspektorat Jendral Kementrian Kesehatan</title> 

    <?php include('include/css.php'); ?>

</head>
<body>

    <div class="boxed_wrapper">

        <?php include ('include/menu.php');?>

        <?= $this->fetch('content') ?>
        
        <?php include('include/footer.php'); ?>

        <!-- Scroll Top Button -->
        <button class="scroll-top tran3s color2_bg">
            <span class="fa fa-angle-up"></span>
        </button>
        <!-- pre loader  -->
        <div class="preloader"></div>

        <?php include('include/js.php'); ?>

    </div>
    
</body>
</html>