<footer class="main-footer">

    <!--Widgets Section-->
    <div class="widgets-section">
        <div class="container">
            <div class="row">
                <!--Big Column-->
                <div class="big-column col-md-12 col-sm-12 col-xs-12">
                    <div class="row clearfix">

                        <!--Footer Column-->
                        <div class="footer-column col-md-4 col-sm-6 col-xs-12">
                            <div class="footer-widget about-widget">
                                <h3 class="footer-title">Kontak Kami</h3>
                                
                                <div class="widget-content">
                                    <ul class="contact-info">
                                    <li><span class="icon-signs"></span> Sekretariat Inspektorat Jenderal <br> 
                                            JL. H. R. Rasuna Said Blok X-5 Kav. 4-9 <br> 
                                            Kuningan - Jakarta Selatan 12950 <br></li>
                                        <li><span class="icon-phone-call"></span> Telp. 021 - 5201590 <br>
                                    Fax. 021 - 5201589 / 5223011 <br></li>
                                        <li><span class="icon-e-mail-envelope"></span>itjen@kemkes.go.id <br>
                                    web.itjenkemkes@gmail.com</li>
                                    </ul>
                                    <?php echo $this->Html->link($this->Html->tag('h4', 'Hubungi Kami'), '/kontak', array('escape'=>false));
                                        ?>
                                </div>
                            </div>
                        </div>
                        <div class="footer-column col-md-4 col-md-offset-0 col-sm-6 col-xs-12">
                            <div class="footer-widget links-widget">
                                <h3 class="footer-title">Link Terkait</h3>
                                <div class="widget-content" style="position: center">
                                    <ul class="list">
                                        <li><a href="service-1.html"><i class="fa fa-check-circle-o"></i>  Business Growth</a></li>
                                        <li><a href="service-2.html"><i class="fa fa-check-circle-o"></i>  Sustainability</a></li>
                                        <li><a href="service-3.html"><i class="fa fa-check-circle-o"></i>  Performance</a></li>
                                        <li><a href="service-4.html"><i class="fa fa-check-circle-o"></i>  Advanced Analytics</a></li>
                                        <li><a href="service-5.html"><i class="fa fa-check-circle-o"></i>  Customer Insights</a></li>
                                        <li><a href="service-6.html"><i class="fa fa-check-circle-o"></i>  Organization</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--Footer Column-->
                        <div class="footer-column col-md-4 col-sm-6 col-xs-12">
                            <div class="footer-widget about-widget">
                                <h3 class="footer-title">Tentang Kami</h3>
                                
                                <div class="widget-content">
                                    <div class="text"><p>The Experts consulting over 20 years of experience we’ll ensure you always get the best guidance. We serve a clients at every level of their organization, in whatever capacity we can be most useful, whether as a trusted advisor.</p> </div>
                                    <div class="link">
                                        <!-- <a href="#" class="default_link">Kontak Kami<i class="fa fa-angle-right"></i></a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Footer Bottom-->
    <section class="footer-bottom">
        <div class="container">
            <div class="pull-left copy-text">
            <p>Copyrights © 2017 <a href="#"> Itjen Kemenkes</a></p>
                
            </div><!-- /.pull-right -->
            <div class="pull-right get-text">
                <ul>
                    <li><a href="#">Support |  </a></li>
                    <li><a href="#">Site Map</a></li>
                </ul>
            </div><!-- /.pull-left -->
        </div><!-- /.container -->
    </section>

</footer>