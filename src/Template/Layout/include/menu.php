<section class="theme_menu stricky">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="main-logo">
                <a href="index.php"><img src="images/logo/kemenkes3.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-7 menu-column">
                <nav class="menuzord" id="main_menu">
                 <ul class="menuzord-menu">
                    <li><a href="#">Profil</a>
                        <ul class="dropdown">
                            <li><?php echo $this->Html->link('sejarah', array('action'=>'sejarah'))?></li>
                            <li><?php echo $this->Html->link('Visi dan Misi', array('action' => 'visimisi')) ?></li>
                            <li><?php echo $this->Html->link('Tugas dan Fungsi', array('action' => 'tugasfungsi')) ?></li>
                            <li><?php echo $this->Html->link('Struktur Organisasi', array('action' => 'strukturorganisasi')) ?></li>
                            <li><?php echo $this->Html->link('Daftar Pejabat', array('action' => 'daftarpejabat')) ?></li>
                            <li><?php echo $this->Html->link('SDM', array('action' => 'sdm')) ?></li>
                        </ul>
                    </li>
                    <li><?php echo $this->Html->link('Berita', array('action' => 'articles')) ?></li>

                    <li><a href="#">Informasi</a>
                        <ul class="dropdown">
                            <li><?php echo $this->Html->link('Daftar Informasi Publik', array('action' => 'majalahinforwas')) ?></li>
                            <li><a href="#">Laporan Kinerja</a></li>
                            <li><?php echo $this->Html->link('Majalah Inforwas', array('action' => 'majalahinforwas')) ?></li>
                            <li><a href="#">Aksi PPK</a></li>
                            <li><?php echo $this->Html->link('Informasi Lain', array('action' => 'majalahinforwas')) ?></li>
                        </ul>
                    </li>

                    <li><a href="#">Tautan</a>
                        <ul class="dropdown">
                            <li><a href="http://itjen.depkes.go.id/wbs/">Whistleblowing Systems</a></li>
                            <li><a href="https://gratifikasi.kemkes.go.id/">Gratifikasi Online</a></li>
                            <li><a href="http://114.6.22.246/">Digital Library</a></li>
                            <li><a href="http://114.6.22.246/">Pengaduan</a></li>
                        </ul>
                    </li>


                    <li><a href="#">Galeri</a>
                        <ul class="dropdown">
                            <li><?php echo $this->Html->link('Galeri Foto', array('action' => 'galerifoto')) ?></li>
                            <li><?php echo $this->Html->link('Galeri Video', array('action' => 'galerivideo')) ?></li>
                            <li><?php echo $this->Html->link('Poster', array('action' => 'galerivideo')) ?></li>
                        </ul>
                    </li>

                    <li><?php echo $this->Html->link('FAQ', array('action' => 'faq')) ?></li>
                </ul><!-- End of .menuzord-menu -->
            </nav> <!-- End of #main_menu -->
        </div>
        <div class="right-column">
            <div class="right-area">
                <div class="nav_side_content">
                    <div class="search_option">
                        <button class="search tran3s dropdown-toggle color1_bg" id="searchDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <form action="#" class="dropdown-menu" aria-labelledby="searchDropdown">
                            <input type="text" placeholder="Search...">
                            <button><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    

</div> <!-- End of .conatiner -->
</section>