<div class="service-sidebar">
    <ul class="service-catergory">
        <li><a href="sejarah-itjen.php">Sejarah</a></li>
        <li class="active"><a href="visi-misi.php">Visi & Misi</a></li>
        <li><a href="tugas-dan-fungsi.php">Tugas & Fungsi</a></li>
        <li><a href="Struktur-organisasi.php">Struktur organisasi</a></li>
        <li><a href="daftar-pejabat.php">Daftar Pejabat</a></li>
        <li><a href="sdm.php">SDM</a></li>

    </ul>

    <br><br>
    <div class="inner-title">
        <h3>Our Brochures</h3>
    </div>
    <div class="brochures">

        <ul class="brochures-lists">
            <li><a href="#"><span class="fa fa-file-pdf-o"></span>Our Presentation.PDF</a></li>
            <li><a href="3"><span class="fa fa-file-word-o"></span>Financial Report.DOC</a></li>
        </ul>
    </div>
</div>