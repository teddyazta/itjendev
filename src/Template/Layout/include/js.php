<!-- jQuery js -->
    
    <?= $this->Html->script('jquery.js') ?>
    <!-- bootstrap js -->
    
    <?= $this->Html->script('bootstrap.min.js') ?>
    <!-- jQuery ui js -->
    
    <?= $this->Html->script('jquery-ui.js') ?>
    <!-- owl carousel js -->
    
    <?= $this->Html->script('owl.carousel.min.js') ?>
    <!-- jQuery validation -->
    
    <?= $this->Html->script('jquery.validate.min.js') ?>

    <!-- mixit up -->
    
    <?= $this->Html->script('wow.js') ?>
    
    <?= $this->Html->script('jquery.mixitup.min.js') ?>
    
    <?= $this->Html->script('jquery.fitvids.js') ?>
    
    <?= $this->Html->script('bootstrap-select.min.js') ?>
    
    <?= $this->Html->script('menuzord.js') ?>

    <!-- revolution slider js -->
    
    <?= $this->Html->script('jquery.themepunch.tools.min.js') ?>
    
    <?= $this->Html->script('jquery.themepunch.revolution.min.js') ?>
    
    <?= $this->Html->script('revolution.extension.actions.min.js') ?>
    
    <?= $this->Html->script('revolution.extension.carousel.min.js') ?>
    
    <?= $this->Html->script('revolution.extension.kenburn.min.js') ?>
    
    <?= $this->Html->script('revolution.extension.layeranimation.min.js') ?>
    
    <?= $this->Html->script('revolution.extension.migration.min.js') ?>
    
    <?= $this->Html->script('revolution.extension.navigation.min.js') ?>
    
    <?= $this->Html->script('revolution.extension.parallax.min.js') ?>
    
    <?= $this->Html->script('revolution.extension.slideanims.min.js') ?>
    
    <?= $this->Html->script('revolution.extension.video.min.js') ?>

    <!-- fancy box -->
    
    <?= $this->Html->script('jquery.fancybox.pack.js') ?>
    
    <?= $this->Html->script('jquery.polyglot.language.switcher.js') ?>
    
    <?= $this->Html->script('nouislider.js') ?>
    
    <?= $this->Html->script('jquery.bootstrap-touchspin.js') ?>
    
    <?= $this->Html->script('SmoothScroll.js') ?>
    
    <?= $this->Html->script('jquery.appear.js') ?>
    
    <?= $this->Html->script('jquery.countTo.js') ?>
    
    <?= $this->Html->script('jquery.flexslider.js') ?>
    
    <?= $this->Html->script('imagezoom.js') ?>
    
    <?= $this->Html->script('default-map.js') ?>
    
    <?= $this->Html->script('bxslider.js') ?>

    <?= $this->Html->script('custom.js') ?>

    <?= $this->Html->script('gmap.js') ?>

    <?= $this->Html->script('https://maps.googleapis.com/maps/api/js?key=AIzaSyCRvBPo3-t31YFk588DpMYS6EqKf-oGBSI') ?>

    <?= $this->Html->script('https://www.google.com/recaptcha/api.js') ?>