<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Peraturan</title> 

    <?php include('include/css.php'); ?>


</head>
<body>

    <div class="boxed_wrapper">

        <?php include('include/menu.php'); ?>

        <div class="inner-banner text-center">
            <div class="container">
                <div class="box">
                    <h3>Peraturan</h3>
                </div><!-- /.box -->
                <div class="breadcumb-wrapper">
                    <div class="clearfix">
                        <div class="pull-right">
                            <ul class="list-inline link-list">
                                <li>
                                    <a href="index.php">Beranda</a>
                        </li><!-- comment for inline hack
                    --><li>
                    Peraturan Menteri Kesehatan
                </li>
            </ul><!-- /.list-line -->
        </div><!-- /.pull-left -->
    </div><!-- /.container -->
</div>
</div><!-- /.container -->
</div>

<section class="service style-2 sec-padd">
    <div class="container"> 
        <div class="row">
        <div class="section-title">
            <h2>Peraturan Menteri Kesehatan</h2>
        </div>
            <div class="col-lg-3 col-md-4 col-sm-12">

                <?php include('include/sidebar-peraturan.php'); ?>
                
            </div>
            <div class="col-lg-9 col-md-8 col-sm-12">
                <div class="outer-box">
                    <div class="intro-img">
                    </div>
                    <div class="inner-title">
                        <h3>Our Brochures</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <div class="accordion-box accordion-style-two">
                                <!--Start single accordion box-->
                                <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                                    <div class="acc-btn">
                                        <p class="title">Permenkes No. 58 Tahun 2016</p>
                                        <div class="toggle-icon">
                                            <span class="plus fa fa-arrow-circle-o-right"></span><span class="minus fa fa-arrow-circle-o-right"></span>
                                        </div>
                                    </div>
                                    <div class="acc-content">
                                        <div class="text"><p>
                                            Tentang Sponsorship bagi Tenaga Kesehatan
                                        </p></div>
                                    </div>
                                </div>
                                <!--Start single accordion box-->
                                <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                                    <div class="acc-btn active">
                                        <p class="title"> Permenkes No. 36 Tahun 2015</p>
                                        <div class="toggle-icon">
                                            <i class="plus fa fa-arrow-circle-o-right"></i><i class="minus fa fa-arrow-circle-o-right"></i>
                                        </div>
                                    </div>
                                    <div class="acc-content collapsed">
                                        <div class="text"><p>
                                            Tentang Pencegahan Fraud JKN
                                        </p></div>
                                    </div>
                                </div>

                                <!--Start single accordion box-->
                                <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                                    <div class="acc-btn">
                                        <p class="title">Permenkes No. 1 Tahun 2015</p>
                                        <div class="toggle-icon">
                                            <i class="plus fa fa-arrow-circle-o-right"></i><i class="minus fa fa-arrow-circle-o-right"></i>
                                        </div>
                                    </div>
                                    <div class="acc-content">
                                        <div class="text"><p>
                                            Tentang Informasi Yang Dikecualikan di Kementerian Kesehatan
                                        </p></div>
                                    </div>
                                </div>

                                <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                                    <div class="acc-btn">
                                        <p class="title">Permenkes No. 6 Tahun 2015</p>
                                        <div class="toggle-icon">
                                            <i class="plus fa fa-arrow-circle-o-right"></i><i class="minus fa fa-arrow-circle-o-right"></i>
                                        </div>
                                    </div>
                                    <div class="acc-content">
                                        <div class="text"><p>
                                            Tentang Kebijakan Pengawasan Inspektorat Jenderal Kementerian Kesehatan
                                        </p></div>
                                    </div>
                                </div>

                                <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                                    <div class="acc-btn">
                                        <p class="title">Permenkes No. 7 Tahun 2014</p>
                                        <div class="toggle-icon">
                                            <i class="plus fa fa-arrow-circle-o-right"></i><i class="minus fa fa-arrow-circle-o-right"></i>
                                        </div>
                                    </div>
                                    <div class="acc-content">
                                        <div class="text"><p>
                                            Tentang Perencanaan dan Penganggaran Bidang Kesehatan
                                        </p></div>
                                    </div>
                                </div>

                                <div class="accordion animated out" data-delay="0" data-animation="fadeInUp">
                                    <div class="acc-btn">
                                        <p class="title">Permenkes No. 11 Tahun 2015</p>
                                        <div class="toggle-icon">
                                            <i class="plus fa fa-arrow-circle-o-right"></i><i class="minus fa fa-arrow-circle-o-right"></i>
                                        </div>
                                    </div>
                                    <div class="acc-content">
                                        <div class="text"><p>
                                            Tentang Petunjuk Teknis Bantuan Operasional Kesehatan
                                        </p></div>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                             <div class="text">
                                <p>Denouncing pleasure and praising pain was born and I will give you a complete account of the system no one rejects, dislikes, or avoids pleasure itself.</p><br>
                                <p>Expound the actually teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful.</p>
                            </div>
                        </div>
                    </div>
                    <br><br>
                </div>
            </div>

        </div>
    </div>
</section>


<?php include('include/footer.php'); ?>

<!-- Scroll Top Button -->
<button class="scroll-top tran3s color2_bg">
    <span class="fa fa-angle-up"></span>
</button>
<!-- pre loader  -->
<div class="preloader"></div>


<!-- jQuery js -->
<script src="js/jquery.js"></script>
<!-- bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- jQuery ui js -->
<script src="js/jquery-ui.js"></script>
<!-- owl carousel js -->
<script src="js/owl.carousel.min.js"></script>
<!-- jQuery validation -->
<script src="js/jquery.validate.min.js"></script>

<!-- mixit up -->
<script src="js/wow.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/bootstrap-select.min.js"></script>
<script src="js/menuzord.js"></script>

<!-- revolution slider js -->
<script src="js/jquery.themepunch.tools.min.js"></script>
<script src="js/jquery.themepunch.revolution.min.js"></script>
<script src="js/revolution.extension.actions.min.js"></script>
<script src="js/revolution.extension.carousel.min.js"></script>
<script src="js/revolution.extension.kenburn.min.js"></script>
<script src="js/revolution.extension.layeranimation.min.js"></script>
<script src="js/revolution.extension.migration.min.js"></script>
<script src="js/revolution.extension.navigation.min.js"></script>
<script src="js/revolution.extension.parallax.min.js"></script>
<script src="js/revolution.extension.slideanims.min.js"></script>
<script src="js/revolution.extension.video.min.js"></script>

<!-- fancy box -->
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.polyglot.language.switcher.js"></script>
<script src="js/nouislider.js"></script>
<script src="js/jquery.bootstrap-touchspin.js"></script>
<script src="js/SmoothScroll.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.countTo.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/imagezoom.js"></script> 
<script src="js/bxslider.js"></script> 
<script id="map-script" src="js/default-map.js"></script>
<script src="js/custom.js"></script>

</div>

</body>
</html>