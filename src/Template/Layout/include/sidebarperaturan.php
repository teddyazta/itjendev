<div class="service-sidebar">
    <ul class="service-catergory">
        <li><?php echo $this->Html->link('Peraturan Pemerintah', array('action'=>'peraturanpemerintah'))?></li>
        <li><?php echo $this->Html->link('Peraturan Presiden', array('action'=>'peraturanpresiden'))?></li>
        <li><?php echo $this->Html->link('Instruksi Presiden', array('action'=>'instruksipresiden'))?></li>
        <li><?php echo $this->Html->link('Peraturan Menteri Kesehatan', array('action'=>'peraturanmenkes'))?></li>
        <li><?php echo $this->Html->link('Keputusan Menteri Kesehatan', array('action'=>'keputusanmenkes'))?></li>
    </ul>

    <br><br>
    <div class="inner-title">
        <h3>Our Brochures</h3>
    </div>
    <div class="brochures">

        <ul class="brochures-lists">
            <li><a href="#"><span class="fa fa-file-pdf-o"></span>Our Presentation.PDF</a></li>
            <li><a href="3"><span class="fa fa-file-word-o"></span>Financial Report.DOC</a></li>
        </ul>
    </div>
</div>