<div class="inner-banner text-center">
    <div class="container">
        <div class="box">
            <h3>Berita</h3>
        </div><!-- /.box -->
        <div class="breadcumb-wrapper">
            <div class="clearfix">
                <div class="pull-right">
                    <ul class="list-inline link-list">
                        <li><a href="../../index.php">Home</a></li>
                        <li><a href="#">Berita</a></li>
                        <li>Detail Berita</li>
                    </ul>
                </div>
            </div><!-- /.container -->
        </div>
    </div><!-- /.container -->
</div>

<div class="sidebar-page-container sec-padd-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12">
                <section class="blog-section">
                    <div class="large-blog-news single-blog-post single-blog wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                        <div class="date">21 <br> Apr</div>
                        <figure class="img-holder">
                            <img src="images/blog/7.jpg" alt="News">
                        </figure>
                        <div class="lower-content">
                            <h4><?= h($article->title) ?></h4>
                            <div class="post-meta">by Stephen Villo</div>
                            <div class="text">
                                <p><?= h($article->body) ?></p>                       
                            </div>
                        </div>
                    </div>
                    <div class="outer-box">

                        <div class="share-box clearfix">
                            <div class="social-box pull-right">
                            <span>Share <i class="fa fa-share-alt"></i></span>
                            <ul class="list-inline social">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12">
                <div class="blog-sidebar">
                    <div class="sidebar_search">
                        <form action="#">
                            <input type="text" placeholder="Search....">
                            <button class="tran3s color1_bg"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </form>
                    </div><br><br> <!-- End of .sidebar_styleOne -->

                    <div class="category-style-one">
                        <div class="inner-title">
                            <h4>Categories</h4>
                        </div>
                        <ul class="list">
                            <li><a href="#" class="clearfix"><span class="float_left">Business Growth </span><span class="float_right">(6)</span></a></li>
                            <li><a href="#" class="clearfix"><span class="float_left">Consulting </span><span class="float_right">(2)</span></a></li>
                            <li><a href="#" class="clearfix"><span class="float_left">Management  </span><span class="float_right">(5)</span></a></li>
                            <li><a href="#" class="clearfix"><span class="float_left">Customer Insights </span><span class="float_right">(10)</span></a></li>
                            <li><a href="#" class="clearfix"><span class="float_left">Organization  </span><span class="float_right">(4)</span></a></li>
                        </ul>
                    </div><br> <!-- End of .sidebar_categories -->

                    <div class="popular_news">
                        <div class="inner-title">
                            <h4>latest news</h4>
                        </div>

                        <div class="popular-post">
                            <div class="item">
                                <div class="post-thumb"><a href="#"><img src="images/blog/post-1.jpg" alt=""></a></div>
                                <a href="#"><h5>Finance & legal <br>throughout project.</h5></a>
                                <div class="post-info"><i class="fa fa-calendar"></i>Jan 08, 2017 </div>
                            </div>
                            <div class="item">
                                <div class="post-thumb"><a href="#"><img src="images/blog/post-2.jpg" alt=""></a></div>
                                <a href="#"><h5>What makes us best <br>in the world? </h5></a>
                                <div class="post-info"><i class="fa fa-calendar"></i>Dec 18, 2016</div>
                            </div>
                            <div class="item">
                                <div class="post-thumb"><a href="#"><img src="images/blog/post-3.jpg" alt=""></a></div>
                                <a href="#"><h5>Why People go with <br>Experts.</h5></a>
                                <div class="post-info"><i class="fa fa-calendar"></i>Nov 21, 2016 </div>
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="sidebar_tags wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <div class="inner-title">
                            <h4>Tag Cloud </h4>

                        </div>

                        <ul>
                            <li><a href="#" class="tran3s">Book</a></li>
                            <li><a href="#" class="tran3s">Company</a></li>
                            <li><a href="#" class="tran3s">Ideas</a></li>
                            <li><a href="#" class="tran3s">Energy</a></li>
                            <li><a href="#" class="tran3s">Engines</a>  </li>
                            <li><a href="#" class="tran3s">Chemical</a></li>
                            <li><a href="#" class="tran3s">Industry</a> </li>
                            <li><a href="#" class="tran3s">Research</a></li>
                        </ul>
                    </div> <!-- End of .sidebar_tags -->

                </div> <!-- End of .wrapper -->   
            </div>
        </div>
    </div>
</div>