<div class="inner-banner text-center">
    <div class="container">
        <div class="box">
            <h3>Berita</h3>
        </div><!-- /.box -->
        <div class="breadcumb-wrapper">
            <div class="clearfix">
                <div class="pull-right">
                    <ul class="list-inline link-list">
                        <li><a href="index.php">Beranda</a></li>
                        <li>Berita</li>
                    </ul>
                </div>
            </div><!-- /.container -->
        </div>
    </div><!-- /.container -->
</div>



<section class="blog-section sec-padd">
    <div class="container">

        <div class="row">
        <?php foreach ($articles as $article): ?>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="default-blog-news wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                    <figure class="img-holder">
                        <a href="blog-details.html"><img src="images/blog/6.jpg" alt="News"></a>
                        <figcaption class="overlay">
                            <div class="box">
                                <div class="content">
                                    <a href="blog-details.html"><i class="fa fa-link" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </figcaption>
                    </figure>
                    <div class="lower-content">
                        <div class="date">21 <br>July</div>
                        <!-- <h4><a href="blog-details.html">Select a small business coach</a></h4> -->
                        <?= $this->Html->link($article->title, ['action' => 'view', $article->id]) ?>
                        <div class="post-meta">by Vincent  |  16 Comments</div>
                        <div class="text">
                            <p><?= h($article->body) ?></p>                            
                        </div>
                        <div class="link">
                            <a href="blog-details.html" class="default_link">Read More <i class="fa fa-angle-right"></i></a>
                            <?= $this->Html->link($this->Html->tag('Read More'), ['action' => 'view', $article->id]);
                                ?>
                        </div>
                        
                    </div>
                </div>
                
            </div>
            <?php endforeach; ?>
        </div>
    

        <ul class="page_pagination center">
            <li><a href="#" class="tran3s"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
            
            <?php echo $this->Paginator->prev("Selengkapnya", array('class' => 'tran3s'))?>
            <li><a href="#" class="active tran3s">1</a></li>
            <li><a href="#" class="tran3s">2</a></li>
            <li><a href="#" class="tran3s"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
            <?php echo $this->Paginator->next("", array('class' => 'tran3s'))?>
        </ul>

        <?= $this->Paginator->prev('« Previous', array('class' => 'tran3s')) ?>
        <?= $this->Paginator->numbers(); ?>
        <?= $this->Paginator->next('Next »') ?>

    </div>
</section>