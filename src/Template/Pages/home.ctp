
<!-- Slider -->
<section class="rev_slider_wrapper">
    <div id="slider1" class="rev_slider"  data-version="5.0">
        <ul>
            
            <li data-transition="fade">
                <img src="images/slider/banner-latest.jpg"  alt="" width="1920" height="683" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption tp-resizeme"
                    data-x="right" data-hoffset="0" 
                    data-y="center" data-voffset="0" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"  
                    data-responsive_offset="on"
                    data-start="500">
                    <div class="slide-content-box">
                        
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="632" 
                    data-y="center" data-voffset="165" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="837" 
                    data-y="center" data-voffset="165" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        
                    </div>
                </div>
            </li>
            <li data-transition="fade">
                <img src="images/slider/banner-latest.jpg"  alt="" width="1920" height="683" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption tp-resizeme" 
                    data-x="center" data-hoffset="0" 
                    data-y="center" data-voffset="-60" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="100">
                    <div class="img-box center">
                        <img src="images/slider/logo.png" alt="">
                    </div>
                </div>
                <div class="tp-caption tp-resizeme"
                    data-x="center" data-hoffset="" 
                    data-y="center" data-voffset="30" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"  
                    data-responsive_offset="on"
                    data-start="500">
                    
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="center" data-hoffset="0" 
                    data-y="center" data-voffset="160" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="1500">
                    <div class="slide-content-box">
                        
                    </div>
                </div>

            </li>
            
            <li data-transition="fade">
                <img src="images/slider/banner-latest.jpg"  alt="" width="1920" height="683" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                
                <div class="tp-caption  tp-resizeme" 
                    data-x="left" data-hoffset="15" 
                    data-y="top" data-voffset="240" 
                    data-transform_idle="o:1;"         
                    data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:3000;e:Power3.easeOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
                    data-mask_in="x:[100%];y:0;s:inherit;e:inherit;" 
                    data-splitin="none" 
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-start="700">
                    
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="15" 
                    data-y="top" data-voffset="470" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2300">
                    <div class="slide-content-box">
                        
                    </div>
                </div>
                <div class="tp-caption tp-resizeme" 
                    data-x="left" data-hoffset="225" 
                    data-y="top" data-voffset="470" 
                    data-transform_idle="o:1;"                         
                    data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                    data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"                     
                    data-splitin="none" 
                    data-splitout="none" 
                    data-responsive_offset="on"
                    data-start="2600">
                    <div class="slide-content-box">
                        
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>
<!-- Slider -->

<!-- Layanan Publik -->
<section class="contact_details sec-padd2">
    <div class="container">
        <div class="section-title center">
            <h2>Layanan utama</h2>
            <div class="text">

            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item center">
                    <div class="icon_box">
                        <span class="icon-magnifying-glass"></span>
                    </div>
                    <h4>Pengaduan</h4>
                    <div class="content center">

                    </div>
                    <a href="#" class="thm-btn2">Selengkapnya</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item center">
                    <div class="icon_box">
                        <span class="icon-clock"></span>
                    </div>
                    <h4>Pelaporan Online</h4>
                    <div class="content center">

                    </div>
                    <a href="#" class="thm-btn2">Selengkapnya</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="item center">
                    <div class="icon_box">
                        <span class="icon-telephone"></span>
                    </div>
                    <h4>Konsultasi</h4>
                    <div class="content center">

                    </div>
                    <a href="#" class="thm-btn2">Selengkapnya</a>
                </div>
            </div>
        </div>

    </div>
</section>
<!-- Layanan Publik -->

<!-- Berita -->
<section class="whychoos-us sec-padd2" style="padding-top: 35px;">
    <div class="container">

        <div class="section-title" style="margin-bottom: 20px;">
            <h2>Berita Terbaru</h2>
            <div class="text">

            </div>
        </div>

        <div class="single-products-details">       
            <div class="product-content-box" style="margin-bottom: 10px;">
                <div class="row">
                    <div class="col-md-7 img-box">
                        <div class="flexslider">
                            <ul class="slides">
                                <li data-thumb="images/tempo/berita-terbaru.jpg">
                                    <div class="thumb-image">
                                        <img src="images/tempo/berita-terbaru.jpg" alt="" class="img-responsive"> 
                                    </div>
                                </li>
                                <li data-thumb="images/tempo/berita-terbaru.jpg">
                                    <div class="thumb-image">
                                        <img src="images/tempo/berita-terbaru.jpg" alt="" class="img-responsive"> 
                                    </div>
                                </li>  
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="default-blog-news wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                            <div class="lower-content" style="border-bottom: 1px solid #10252e;">
                                <div class="date">21 <br>April</div>
                                <?php echo $this->Html->link($this->Html->tag('h4', 'Retail banks wake up to digital'), '/berita_detail', array('escape'=>false));
                                ?>
                                <!-- <h4><a href="#">Retail banks wake up to digital</a></h4> -->
                                <div class="text">
                                    <p>know how to pursue pleasure rationally seds encounter consequences.</p>               
                                </div>
                            </div>
                            <div class="lower-content" style="border-bottom: 1px solid #10252e;">
                                <div class="date">21 <br>April</div>
                                <h4><a href="#">Retail banks wake up to digital</a></h4>
                                <div class="text">
                                    <p>know how to pursue pleasure rationally seds encounter consequences.</p>               
                                </div>
                            </div>
                            <div class="lower-content" style="border-bottom: 1px solid #10252e;">
                                <div class="date">21 <br>April</div>
                                <h4><a href="#">Retail banks wake up to digital</a></h4>
                                <div class="text">
                                    <p>know how to pursue pleasure rationally seds encounter consequences.</p>               
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>   
            </div>

        </div> 

    </div>
</section>
<!-- Berita -->

<!-- Unduh Buku -->
<section class="latest-project sec-padd" style="padding: 40px 0 60px; background: #ffffff;">
    <div class="container">
        <div class="section-title">
            <h2>Unduh Buku</h2>
        </div>
        <div class="latest-project-carousel">
            <div class="item">
                <div class="single-project">
                    <figure class="imghvr-shutter-in-out-horiz">
                        <img src="images/tempo/unduh-buku1.jpg" alt="Awesome Image"/>
                        <figcaption>
                            <div class="content">
                                <a href="#"><h4>Latest Technology</h4></a>
                                <p>Consulting</p>
                            </div> 
                        </figcaption>
                    </figure>
                </div>
            </div>
            <div class="item">
                <div class="single-latest-project-carousel">
                    <div class="single-project">
                        <figure class="imghvr-shutter-in-out-horiz">
                            <img src="images/tempo/unduh-buku2.jpg" alt="Awesome Image"/>
                            <figcaption>
                                <div class="content">
                                    <a href="#"><h4>Audit & Assurance</h4></a>
                                    <p>Financial</p>
                                </div>    
                            </figcaption>
                        </figure>
                    </div>
                </div><!-- /.single-latest-project-carousel -->
            </div>
            <div class="item">
                <div class="single-latest-project-carousel">
                    <div class="single-project">
                        <figure class="imghvr-shutter-in-out-horiz">
                            <img src="images/tempo/unduh-buku3.jpg" alt="Awesome Image"/>
                            <figcaption>
                                <div class="content">
                                    <a href="#"><h4>Business Growth</h4></a>
                                    <p>Growth</p>
                                </div> 
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="single-latest-project-carousel">
                    <div class="single-project">
                        <figure class="imghvr-shutter-in-out-horiz">
                            <img src="images/tempo/unduh-buku4.jpg" alt="Awesome Image"/>
                            <figcaption>
                                <div class="content">
                                    <a href="#"><h4>Transporation Service</h4></a>
                                    <p>Marketing</p>
                                </div> 
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>            
        </div>

    </div>
</section>
<!-- Unduh Buku -->

<!-- Peraturan -->
<section class="service-style3 sec-padd2" style="padding: 40px 0 60px;">
    <div class="container">
        <div class="section-title">
            <h2>Peraturan</h2>
        </div>
        <div class="service_carousel">
            <div class="item">

                <div class="bottom-content">

                    <div class="icon_box">
                        <span class="icon-money2"></span>
                    </div>
                    <a href="peraturan-pemerintah.php"><h4>Peraturan Pemerintah</h4></a>
                    <p class="title">Trusted</p>    
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                            <span class="icon-money2"></span>
                        </div>
                            <?php echo $this->Html->link($this->Html->tag('h4', 'Peraturan Pemerintah'), '/peraturanpemerintah', array('escape'=>false));
                            ?>
                        <p class="title">Trusted</p> 
                        <div class="text">
                            <p>Electronic Materials has servd the semicondctor industry as a leading-edge materials.</p>
                        </div>
                    </div>

                </div>  
                
            </div>
            <div class="item">

                <div class="bottom-content">

                    <div class="icon_box">
                        <span class="icon-landscape"></span>
                    </div>
                    <a href="peraturan-presiden.php"><h4>Peraturan Presiden</h4></a>
                    <p class="title">Large Scale</p>    
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                            <span class="icon-landscape"></span>
                        </div>
                        <?php echo $this->Html->link($this->Html->tag('h4', 'Peraturan Presiden'), '/peraturanpresiden', array('escape'=>false));
                            ?>
                        <p class="title">Large Scale</p> 
                        <div class="text">
                            <p>Electronic Materials has servd the semicondctor industry as a leading-edge materials.</p>
                        </div>
                    </div>

                </div>  
                
            </div>
            <div class="item">

                <div class="bottom-content">

                    <div class="icon_box">
                        <span class="icon-medical"></span>
                    </div>
                    <a href="instruksi-presiden.php"><h4>Instruksi Presiden</h4></a>
                    <p class="title">Corporation</p>    
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                            <span class="icon-medical"></span>
                        </div>
                        <?php echo $this->Html->link($this->Html->tag('h4', 'Instruksi Presiden'), '/instruksipresiden', array('escape'=>false));
                            ?>
                        <p class="title">Corporation</p> 
                        <div class="text">
                            <p>Electronic Materials has servd the semicondctor industry as a leading-edge materials.</p>
                        </div>
                    </div>

                </div>  
                
            </div>
            <div class="item">

                <div class="bottom-content">

                    <div class="icon_box">
                        <span class="icon-transport"></span>
                    </div>
                    <a href="peraturan-menkes.php"><h4>Peraturan Menteri Kesehatan</h4></a>
                    <p class="title">Very Economy</p>    
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                            <span class="icon-transport"></span>
                        </div>
                        <?php echo $this->Html->link($this->Html->tag('h4', 'Peraturan Menteri Kesehatan'), '/peraturanmenkes', array('escape'=>false));
                            ?>
                        <p class="title">Very Economy</p> 
                        <div class="text">
                            <p>Electronic Materials has servd the semicondctor industry as a leading-edge materials.</p>
                        </div>
                    </div>

                </div>  
                
            </div>
            <div class="item">

                <div class="bottom-content">

                    <div class="icon_box">
                        <span class="icon-transport"></span>
                    </div>
                    <a href="keputusan-menkes.php"><h4>Keputusan Menteri Kesehatan</h4></a>
                    <p class="title">Very Economy</p>    
                </div>
                <div class="overlay-box">
                    <div class="inner-box">
                        <div class="icon_box">
                            <span class="icon-transport"></span>
                        </div>
                        <?php echo $this->Html->link($this->Html->tag('h4', 'Keputusan Menteri Kesehatan'), '/keputusanmenkes', array('escape'=>false));
                            ?>
                        <p class="title">Very Economy</p> 
                        <div class="text">
                            <p>Electronic Materials has servd the semicondctor industry as a leading-edge materials.</p>
                        </div>
                    </div>

                </div>  
                
            </div>

        </div>

    </div>
</section>
<!-- Peraturan -->


